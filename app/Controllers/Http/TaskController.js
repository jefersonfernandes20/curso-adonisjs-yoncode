'use strict'

class TaskController {
  index({ view }) {
    const tasks = [
      {
        title: 'Task one',
        body: 'This is task one'
      },
      {
        title: 'Task two',
        body: 'this is task two'
      }
    ]

    return view.render('task', {
      title: 'Your tasks',
      tasks // ou tasks(ou qualquer outro nome: tasks)
    })
  }
}

module.exports = TaskController
